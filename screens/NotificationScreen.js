import React from 'react';
import { Alert, AsyncStorage, ScrollView, StyleSheet, Header, View, ListView, RefreshControl, Text, TouchableWithoutFeedback} from 'react-native';
import { Constants } from 'expo';

export default class NotificationScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isRefreshing: false,
            loaded: 0,
            tasks: []
        };

        this._getTasks();
    }

    static navigationOptions = {
        title: 'Notifications',
    };

    render() {
        const rows = this.state.tasks.map((row, ii) => {
            return <Row key={ii} data={row}/>;
        });
        return (
        <ScrollView
            style={styles.scrollview}
            refreshControl={<RefreshControl refreshing={this.state.isRefreshing} onRefresh={this._onRefresh} tintColor="darkgray" title="Loading..." titleColor="darkgray" colors={['#ff0000', '#00ff00', '#0000ff']} progressBackgroundColor="#ffff00"/>}
            >
            {rows}
        </ScrollView>
        );
    }

    _onRefresh = () => {
        this.setState({isRefreshing: true});
        this._getTasks();
    };

    _getTasks() {
        AsyncStorage.getItem('user').then((user) => {
            var user = JSON.parse(user);
            fetch(Constants.manifest.extra.apiHost + `/api/v2/tasks?user_id=${user.id}&expand=action`, {
                method: 'GET',
                headers: { 
                    'Authorization': 'Bearer ' + user.auth_key
                }
            })
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({tasks: responseJson});
                this.setState({isRefreshing: false});
            })
            .done();
        })
    }
}

class Row extends React.Component {
    render() {
        return (
        <TouchableWithoutFeedback>
            <View style={styles.row}>
                <Text style={styles.text}>
                    {this.props.data.action.name + ' (' + this._convertTimestamp(this.props.data.created_at) + ')'}
                </Text>
            </View>
        </TouchableWithoutFeedback>
        );
    }

    _convertTimestamp(timestamp) {
        var d = new Date(timestamp * 1000),	// Convert the passed timestamp to milliseconds
            yyyy = d.getFullYear(),
            mm = ('0' + (d.getMonth() + 1)).slice(-2),	// Months are zero based. Add leading 0.
            dd = ('0' + d.getDate()).slice(-2),			// Add leading 0.
            hh = d.getHours(),
            h = hh,
            min = ('0' + d.getMinutes()).slice(-2),		// Add leading 0.
            ampm = 'AM',
            time;
                
        if (hh > 12) {
            h = hh - 12;
            ampm = 'PM';
        } else if (hh === 12) {
            h = 12;
            ampm = 'PM';
        } else if (hh == 0) {
            h = 12;
        }
        
        // ie: 2013-02-18, 8:35 AM	
        time = yyyy + '-' + mm + '-' + dd + ', ' + h + ':' + min + ' ' + ampm;
            
        return time;
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 15,
        backgroundColor: '#fff',
    },
    row: {
        borderColor: 'darkgray',
        borderBottomWidth: 1,
        padding: 20,
        margin: 5,
    },
    text: {
        alignSelf: 'flex-start',
    },
    scrollview: {
        flex: 1,
    }
});