import React, {Component} from 'react';
import { AsyncStorage, Image, Platform, ScrollView, StyleSheet, Text, TouchableOpacity, View, Vibration} from 'react-native';
import { Constants, WebBrowser } from 'expo';
import { MonoText } from '../components/StyledText';
import Swipeable from 'react-native-swipeable';

const DURATION = 1000
const PATTERN = [1000, 2000, 3000]

export default class HomeScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isRefreshing: false,
            currentlyOpenSwipeable: null,
            leftActionActivated: false,
            actions:[]
        };

        this._getActions(); 
    }

    static navigationOptions = {
        // header: null,
        title : 'Home'
    };

    render() {
        const {currentlyOpenSwipeable} = this.state;
        const {leftActionActivated} = this.state;
        const itemProps = {
            onOpen: (event, gestureState, swipeable) => {
                if (currentlyOpenSwipeable && currentlyOpenSwipeable !== swipeable) {
                    currentlyOpenSwipeable.recenter();
                }
                this.setState({currentlyOpenSwipeable: swipeable});
            },
            onClose: () => this.setState({currentlyOpenSwipeable: null})
        };

        return (
            <ScrollView onScroll={this._handleScroll()} style={styles.container} contentContainerStyle={styles.contentContainer}>
                {this.state.actions.map((action, index) => (
                <Swipeable
                key={action.action.id}
                leftActionActivationDistance={150}
                leftContent={(
                    <View style={[styles.leftSwipeItem, {backgroundColor: leftActionActivated ? 'lightgoldenrodyellow' : 'steelblue'}]}>
                    {leftActionActivated ?
                        <Text>DONE !</Text> :
                        <Text>PULL !</Text>}
                    </View>
                )}
                onLeftActionActivate={() => {
                    this.setState({leftActionActivated: true});
                    this._handleSwipeOrPress(action.action);
                }}
                onLeftActionDeactivate={() => this.setState({leftActionActivated: false})} 
                >
                <TouchableOpacity onLongPress={() => {this._handleSwipeOrPress(action.action)}}>
                    <View style={[styles.listItem, {backgroundColor: 'white'}]} >
                        <Text >{action.action.name}</Text>
                    </View>
                </TouchableOpacity>
                </Swipeable>
                ))}
            </ScrollView>
        );
    }

    _handleScroll = () => {
        const {currentlyOpenSwipeable} = this.state;
        if (currentlyOpenSwipeable) {
            currentlyOpenSwipeable.recenter();
            this._fetchData()
        }
    };

    _getActions() {
        AsyncStorage.getItem('user').then((user) => {
            var user = JSON.parse(user); 
            fetch(Constants.manifest.extra.apiHost + `/api/v2/user-actions?user_id=${user.id}&expand=action`, {
                method: 'GET',
                headers: { 'Authorization': 'Bearer ' + user.auth_key }
            })
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({actions: responseJson});
                this.setState({isRefreshing: false});
            })
            .done();
        })
    }

    _handleSwipeOrPress = (action) => {
        AsyncStorage.getItem('user').then((user) => {
            var user = JSON.parse(user);
            console.log("Access Token TOP " + user);
            console.log(action);
            console.log("Access Token BOTTOM "); 
            fetch(Constants.manifest.extra.apiHost + `/api/v2/task/complete?access-token=${user.auth_key}`, {
                method: 'POST',
                headers: { 
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    action_id: action.id,
                    user_id: user.id, 
                    team_id: 1,
                    comments: ' '
                })
            })
            .then((response) => response.json())
            .then((responseJson) => {
                Vibration.vibrate(DURATION);
                console.log(responseJson);
            })
            .done();
        });
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    listItem: {
        height: 70,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderBottomColor: 'darkgray'
    },
    leftSwipeItem: {
        flex: 1,
        alignItems: 'flex-end',
        justifyContent: 'center',
        paddingRight: 20
    },
    contentContainer: {
        paddingTop: 0,
    }
});
