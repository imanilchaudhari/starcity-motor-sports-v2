import React from 'react';
import { AsyncStorage, Alert, AppRegistry, Image, StyleSheet, TextInput, TouchableHighlight, TouchableOpacity, ScrollView, ViewAndroid, View, Text} from 'react-native';
import { NavigationActions } from 'react-navigation';
import { Constants } from 'expo';
import { RkButton, RkText, RkTextInput, RkAvoidKeyboard, RkStyleSheet, RkTheme } from 'react-native-ui-kitten'; // Import React-Native UI Kitten Design
import KeyboardSpacer from '../components/KeyboardSpacer';
import Spinner from '../components/Spinner';

export default class LoginScreen extends React.Component {
	constructor(props) {
		super(props);
		const {navigation} = props;
		this.state = {
			visible: false,
			signedIn : false,
			navigation: navigation,
			username : null,
			password : null,
			hidePassword: true
		}
	}

	static navigationOptions = {
		header: null
	};

	componentWillMount(){
		var value =  AsyncStorage.getItem('user');
		value.then((user) => {
			if (user !== null){
				console.log(value);
				this.setState({
					signedIn: true
				})
			}
		});
	}

	managePasswordVisibility = () =>
	{
		this.setState({ hidePassword: !this.state.hidePassword });
	}

	render() {
        const { navigation } = this.props.navigation;
		const { manifest } = Constants;
		var user = AsyncStorage.getItem('user');
		if (this.state.signedIn) {
			return (
				<View style={styles.container}>
					<Spinner visible={this.state.visible} textContent={"Processing..."} textStyle={{color: '#FFF'}} />
	
					<Image style={styles.image} source={require('../assets/icon.png')}/>
	
					<RkButton onPress={() => { this._handleContinue(); }} rkType='large' style={styles.save}>
						Continue
					</RkButton>	
					<KeyboardSpacer/>
				</View>
			);
		} else {
			return (
				<View style={styles.container}>
					<Spinner visible={this.state.visible} textContent={"Processing..."} textStyle={{color: '#FFF'}} />
	
					<Image style={styles.image} source={require('../assets/icon.png')}/>
	
					<RkTextInput rkType='rounded' placeholder='Username or Email Address' value={this.props.username} onChangeText={(username) => this.setState({username : username})}/>
	
					<RkTextInput rkType='rounded' placeholder='Password' value={this.props.password} secureTextEntry={this.state.hidePassword} onChangeText={(password) => this.setState({password : password})}/>
					
					<TouchableOpacity activeOpacity = { 0.8 } style = { styles.visibilityBtn } onPress = { this.managePasswordVisibility }>
						<Image source = { ( this.state.hidePassword ) ? require('../assets/hide.png') : require('../assets/view.png') } style = { styles.btnImage } />
					</TouchableOpacity>
	
					<RkButton onPress={() => { this._handleLogin(); }} rkType='large' style={styles.save}>
						LOGIN
					</RkButton>
					
					<RkText onPress={() => { this._handlePasswordReset(); }}>
						Forget Password?
					</RkText>
	
	
					{/* <TouchableOpacity style={styles.buttonWrapper} onPress={this.userSignup.bind(this)}>
						<Text style={styles.buttonText}> Sign Up </Text>
					</TouchableOpacity> */}
					<KeyboardSpacer/>
				</View>
			);
		}
	}

	_handleLogin = async () => { // The "async" serve for the await function
		console.log('Login button pressed');
		try {
			if (this.state.username == null) {
				Alert.alert("Please enter your username");
				return;
			}

			if (this.state.password == null) {
				Alert.alert("Please enter your password");
				return;
			}
			this.setState({ visible: !this.state.visible });
			const response = await fetch(Constants.manifest.extra.apiHost + `/api/v2/users/login`, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify({
					username: this.state.username,
					password: this.state.password
				})
			});

			if (response.status === 200){
				const responseJson = await response.json();
				await AsyncStorage.setItem('user', JSON.stringify(responseJson)); 
				this.setState({ signedIn: true });				
				this._navigate('Main');
			} else if (response.status === 400){
				Alert.alert('Wrong username or password.');
				this.setState({ visible: !this.state.visible });
			} else{
				Alert.alert('Server Unavailable');
				this.setState({ visible: !this.state.visible });
			}
		}
		catch (error) {
			console.log(error);
		}
  	}

  	_handlePasswordReset = async () => {
		try {
			console.log('Password Reset button pressed');
			if (this.state.username == null) {
				console.log("Please enter your username or email address.");
				Alert.alert("Please enter your username or email address.");
				return;
			}
			this.setState({ visible: !this.state.visible });
			const response = await fetch(Constants.manifest.extra.apiHost + "/api/v2/users/request-password-reset", {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify({
					username: this.state.username
				})
			});

			if (response.status === 200){
				const data = await response.json();
				Alert.alert(data.message);
				this.setState({ visible: !this.state.visible });
			} else if (response.status === 400){
				const data = await response.json();
				Alert.alert(data.message);
				this.setState({ visible: !this.state.visible });				
			} else{
				const responseJson = await response.json();
				Alert.alert(responseJson.message);
				this.setState({ visible: !this.state.visible });				
			}
		}
		catch (error) {
			console.log(error)
		}
	}
	  
	_navigate(targetRoute) {
		const resetAction = NavigationActions.reset({
			index: 0,
			actions: [
				NavigationActions.navigate({ routeName: targetRoute }),
			],
		});
		this.props.navigation.dispatch(resetAction);
	}

	_handleContinue = async () => { // The "async" serve for the await function
		console.log('Continue button pressed'); 
		this._navigate('Main');
  	}

	async saveItem(item, selectedValue) {
        try {
            await AsyncStorage.setItem(item, selectedValue);
        } catch (error) {
            console.error('AsyncStorage error: ' + error.message);
        }
    }

    userSignup() {
        if (!this.state.username || !this.state.password) return;
        // TODO: localhost doesn't work because the app is running inside an emulator. Get the IP address with ifconfig.
        fetch(Constants.manifest.extra.apiHost + '/api/v2/users/signup', {
            method: 'POST',
            headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
            body: JSON.stringify({
                username: this.state.username,
                password: this.state.password,
            })
        })
        .then((response) => response.json())
        .then((responseJson) => {
            Alert.alert( 'Signup Success!', 'Click the button to get a Chuck Norris quote!')
            // redirect
        })
        .done();
    }

    userLogin() {
        if (!this.state.username || !this.state.password) return;
        // TODO: localhost doesn't work because the app is running inside an emulator. Get the IP address with ifconfig.
        fetch(Constants.manifest.extra.apiHost + '/api/v2/users/login', {
            method: 'POST',
            headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' },
            body: JSON.stringify({
                username: this.state.username,
                password: this.state.password,
            })
        })
        .then((response) => response.json())
        .then((responseJson) => {
            Alert.alert('Login Success!', 'Click the button to get a Chuck Norris quote!')
            // redirect
        })
        .done();
    }
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#ddd',
	},
	title: {
		marginTop: 40,
		marginBottom: 10,
		fontSize: 20,
		fontWeight: 'bold',
	},
	reloadButton: {
		fontSize: 16,
		fontWeight: 'bold',
		backgroundColor: 'blue',
		color: 'white',
		padding: 10,
	},
	loginField: {
		height: 40,
		borderColor: 'gray',
		borderWidth: 1,
		margin: 10,
		padding: 10,
		backgroundColor: 'white',
	},
	passwordField: {
		height: 40,
		borderColor: 'gray',
		borderWidth: 1,
		margin: 10,
		padding: 10,
		backgroundColor: 'white',
	},
	listView: {
		alignSelf: 'stretch',
		backgroundColor: 'white',
	},
	listViewRow: {
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center',
		height: 50,
	},
	rowName: {
		padding: 10,
		fontSize: 16,
	},
	rowTemperature: {
		padding: 10,
		fontSize: 16,
	},
	save: {
		marginVertical: 19
	},
	image: {
		width: 150,
		height: 150
	},
	visibilityBtn:
	{
		position: 'relative',
		marginTop : -55,
		marginLeft : "75%",
		right: 3,
		height: 40,
		width: 35,
		padding: 5
	},
	btnImage:
	{
		resizeMode: 'contain',
		height: '100%',
		width: '100%'
	}
});
